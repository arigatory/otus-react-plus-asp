# Otus Weather 

Запуск приложения API

```bash
cd task_2/API
dotnet run
```

Запуск приложения React

```bash
cd task_2/client-app
npm i
npm start
```

