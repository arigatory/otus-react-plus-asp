import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';
import { Container, Dimmer, Icon, Loader, Statistic } from 'semantic-ui-react';
import Header from 'semantic-ui-react/dist/commonjs/elements/Header';


function App() {
  const [weather, setWeather] = useState([]);
  useEffect(() => {
    axios.get('https://localhost:7011/WeatherForecast')
      .then((response) => {
        setWeather(response.data);
      });
  }, []);
  return (
    <Container>
      <Header as="h2" icon textAlign='center' color='green'>
        <Icon name="sun" />
        Погода
      </Header>
      <div className="ui cards">
        {weather ? weather.map((info: any) => {
          return (
            <div className="ui card">
              <div className="content">
                <div className="header">{info.summary}</div>
                <Statistic color={info.temperatureC > 9 ? 'red' : 'blue'}>
                  <Statistic.Value>{info.temperatureC} °C</Statistic.Value>
                  <Statistic.Label>Температура</Statistic.Label>
                </Statistic>
                <div className="description">
                  {info.date}
                </div>
              </div>
            </div>
          );
        }) :
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>

        }
      </div>
    </Container>
  );
}

export default App;
